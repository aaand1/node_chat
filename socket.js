'use strict'

var q = require('q'),
  db = require('./db/database'),
  usersRepo = require('./db/users'),
  roomsRepo = require('./db/rooms'),
  messagesRepo = require('./db/messages')

module.exports = function (app) {
  var server = require('http').Server(app);
  var io = require('socket.io').listen(server);

  server.listen(3000);

  var chat = io.of('/chat')
  //  chat.use(function (socket, next) {
  //    var name = socket.request.headers.cookie.uid
  //    if (name) {
  //      return next()
  //    }
  //    next(new Error('Authentication error'))
  //  })

  chat.on('connection', function (socket) {
    socket.on('userJoin', function (data) {
      assertRoom(data)

      var userId = data.userId,
        roomId = data.roomId

      socket.join(roomId)

      usersRepo.getUser(userId).then(function (user) {
        usersRepo.toggleUserRoom(userId, roomId, true, true)

        if (user.rooms.indexOf(roomId) > -1) {
          socket.broadcast.to(roomId).emit('userOnline', {userId: userId})
        } else {
          socket.broadcast.to(roomId).emit('userJoined', user)
        }

      })

      roomsRepo.toggleUserToRoom(userId, roomId, true)
    })

    socket.on('userLeave', function (data) {
      assertRoom(data)

      usersRepo.toggleUserRoom(data.userId, data.roomId, false, false)
      roomsRepo.toggleUserToRoom(data.userId, data.roomId, false)

      socket.broadcast.to(data.roomId).emit('userLeft', {userId: data.userId});
    })

    socket.on('newMessage', function (data) {
      assertRoom(data)

      messagesRepo.insertMessage({
        body: data.body,
        userId: data.userId,
        roomId: data.roomId,
        date: new Date()
      }).then(function (messageResult) {
        socket.broadcast.to(data.roomId).emit('newMessage', messageResult);
        socket.emit('newMessage', messageResult);
      })
    })

    socket.on('disconnect', function (data) {
      //assertRoom(data)
      //
      //TODO: possibly clear all users by roomId if any available
      ////TODO: prevent remove from activeRooms for current user's disconnect from another machine
      //usersRepo.toggleUserRoom(data.userId, data.roomId, true, false)
      //  .then(function (user) {
      //    socket.broadcast.to(data.roomId).emit('userOffline', data.userId);
      //  })
    })
  })

  function assertRoom(data) {
    if (!data.roomId) {
      throw 'invalid room'
    }
  }
}
