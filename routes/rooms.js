'use strict'

var express = require('express');
var router = express.Router();

var q = require('q'),
  db = require('../db/database'),
  roomsRepo = require('../db/rooms')

router.get('/:id?', function (req, res) {
  var id = req.params.id,
    idResult = id ? db.toObjectId(id) : q.resolve(null)

  q.allSettled([idResult]).spread(function (roomIdResult) {
    var roomId = roomIdResult.state == 'rejected' ? -1 : roomIdResult.value
    if (roomId == null) {
      return roomsRepo.find({})
    }

    return roomsRepo.findOne({
      $or: [
        {
          _id: roomId
        },
        {
          name: id
        }
      ]
    })
  }).then(function (roomsResult) {
    res.status(200).send(roomsResult)
  }).catch(function (reason) {
      res.status(500).send(reason)
    })
});

router.post('/', function (req, res) {
  var name = req.body.name

  if (!name) {
    return res.status(400).send({error: 'invalid'})
  }

  roomsRepo.insert({
    name: name,
    messages: [],
    users: []
  }).then(function (insertResult) {
    res.status(200).send(insertResult[0])
  }).catch(function (reason) {
    res.status(500).send(reason)
  })
});

router.post('/delete/:id', function (req, res) {
  var id = req.params.id

  db.toObjectId(id)
    .then(function (roomId) {
      return roomsRepo.remove({_id: roomId})
    })
    .then(function (removeResult) {
      res.status(200).send(removeResult)
    }).catch(function (reason) {
      res.status(500).send(reason)
    })
})

module.exports = router;
