'use strict'

var db = require('../db/database'),
  q = require('q')

var express = require('express')
var router = express.Router()

router.post('/login', function (req, res) {
  var login = req.body.login

  if (!login) {
    return res.status(400).send({error: 'invalid'})
  }

  db.from('users').findOne({login: login}).then(function (user) {
    if (user) {
      return [user]
    }

    return db.from('users').findAndModify(
      {login: login},
      db.ID_SORT,
      {$set: {login: login, rooms: [], activeRooms: []}},
      {new: true, upsert: true}
    )
  }).then(function (findResult) {
    var user = findResult[0] || null
    var ONE_DAY = 24 * 60 * 60 * 1000
    res.cookie('uid', user._id + '', {httpOnly: true, expires: new Date(Date.now() + ONE_DAY)})
    res.status(200).send(user)
  }).catch(function (reason) {
    res.status(400).send(reason)
  })
})

router.post('/logout', function (req, res) {
  res.clearCookie('uid')
  res.sendStatus(200)
})

router.get('/me', function (req, res) {
  var uid = req.cookies.uid || null,
    uidResult = db.toObjectId(uid)

  uidResult.then(function (userId) {
    return db.from('users').findOne(
      {_id: userId}
    ).then(function (result) {
        if (!result) {
          return q.reject(result)
        }
        return result
      })
  }).then(function (user) {
    res.status(200).send(user)
  }).catch(function (reason) {
    res.status(401).send('unauthorized')
  })
})

module.exports = router
