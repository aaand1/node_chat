'use strict'

var q = require('q'),
  db = require('./database')

exports.find = function (query) {
  return db.from('rooms').find(query)
}

exports.findOne = function (query) {
  return db.from('rooms').findOne(query)
    .then(enrichRoom)
}

exports.insert = function (data) {
  return db.from('rooms').insert(data)
}

exports.upsert = function (findQuery, upsertQuery) {
  return db.from('rooms').findAndModify(
    findQuery,
    db.ID_SORT,
    upsertQuery,
    {new: true, upsert: true}
  ).then(db.first)
}

exports.remove = function (query) {
  return db.from('rooms').remove(query)
}

exports.toggleUserToRoom = function (userId, roomId, add) {
  var updateQuery = {}
  updateQuery[add ? '$addToSet' : '$pull'] = { users: userId }

  return db.from('rooms').findAndModify(
    {_id: db.ObjectID(roomId)},
    db.ID_SORT,
    updateQuery
  ).then(db.first)
}

function enrichRoom(room) {

  return q.all([
    getUsers(),
    getMessages()
  ]).spread(function (usersResult, messagesResult) {
    var usersIndex = db.indexById(usersResult)

    room.users = room.users.map(function (uId) {
      return usersIndex[uId] || null
    })

    room.messages = messagesResult || []

    return room
  })

  function getUsers() {
    return q(room.users).then(function () {
      return db.from('users').find({
        _id: {$in: room.users.map(db.ObjectID)}
      })
    }).catch(returnEmpty)
  }

  function getMessages() {
    return db.from('messages').find({
      roomId: room._id+''
    }).catch(returnEmpty)
  }

  function returnEmpty() {
    return []
  }
}
