var q = require('q'),
  db = require('./database'),
  usersRepo = require('./users')

exports.find = function (query) {
  return db.from('messages').find(query)
}

exports.findOne = function (query) {
  return db.from('messages').findOne(query)
}

exports.addMessage = function (data) {
  return db.from('messages').findAndModify(
    {},
    db.ID_SORT,
    data,
    {new: true, upsert: true}
  ).then(db.first)
}

exports.insertMessage = function (data) {
  return db.from('messages').insert(
    data
  ).then(db.first).then(enrichMessage)
}

exports.remove = function (query) {
  return db.from('messages').remove(query)
}

function enrichMessage(message) {
  return usersRepo.getUser(message.userId).then(function (user) {
    message.user = user
    return message
  })
}
