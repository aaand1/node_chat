'use strict'

var q = require('q'),
  mongodb = require('mongodb'),
  client = mongodb.MongoClient

var db = null

exports.connect = function (app) {
  var defer = q.defer()

  client.connect('mongodb://localhost/node_chat', function (err, dbInstance) {
    if (err) {
      return defer.reject(err)
    }
    defer.resolve(db = dbInstance)
  })

  return defer.promise
}

exports.from = promisedCollection

exports.ID_SORT = ['_id', 'asc']

exports.toObjectId = function (id) {
  return id ? q.when(id)
    .then(ObjectId)
    .catch(reject) : reject()

  function reject() {
    return q.reject('invalid ObjectId')
  }
}

exports.ObjectID = ObjectId

exports.first = function (result) {
  return result[0]
}

exports.indexById = function (array, indexField) {
  var indexedObject = {};
  for (var i = 0, l = array.length; i < l; i++) {
    var e = array[i];
    indexedObject[e[indexField || '_id']] = e;
  }
  indexedObject._array = array;
  return indexedObject;
}

function promisedCollection(name) {
  var collection = db.collection(name)

  return {
    collection: collection,
    find: function () {
      var args = arguments
      return q(true).then(function () {
        var cursor = collection.find.apply(collection, args)
        return q.npost(cursor, 'toArray')
      })
    },
    findOne: function () {
      var args = arguments
      return q.npost(collection, 'findOne', args)
    },
    findAndModify: function () {
      var args = arguments
      return q.npost(collection, 'findAndModify', args)
    },
    insert: function () {
      var args = arguments
      return q.npost(collection, 'insert', args)
    },
    update: function () {
      var args = arguments
      return q.npost(collection, 'update', args)
    }
  }
}

function ObjectId(value) {
  if (this instanceof ObjectId) {
    throw new TypeError('ObjectId is not a constructor, should call as function')
  }
  return value instanceof mongodb.ObjectID ? value : mongodb.ObjectID(value)
}
