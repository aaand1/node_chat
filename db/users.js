'use strict'

var q = require('q'),
  db = require('./database')

exports.toggleUserRoom = function (userId, roomId, addToRooms, addToActive) {
  var updateQuery = {},
    updateQueryValue = {activeRooms: roomId}

  if (addToRooms === addToActive) {
    updateQueryValue['rooms'] = roomId
  }
  updateQuery[addToRooms ? '$addToSet' : '$pull'] = updateQueryValue

  return db.from('users').findAndModify(
    {_id: db.ObjectID(userId)},
    db.ID_SORT,
    updateQuery
  ).then(db.first)
}

exports.getUser = function (userId) {
  return db.toObjectId(userId).then(function (id) {
    return db.from('users').findOne({_id: id})
  })
}
