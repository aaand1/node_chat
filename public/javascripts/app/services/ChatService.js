(function (module) {
  'use strict';

  module.service('ChatService', [
    'socketFactory', '$q', '$rootScope',
    function (socketFactory, q, rootScope) {
      var socket = null

      return {
        joinRoom: function (roomId, socketOptions) {
          if (!rootScope.hasUser()) {
            return q.reject('no active user. can\'t enter anonymously')
          }

          var defer = q.defer()
          if (!socket) {
            socket = socketFactory({
              ioSocket: window.io.connect('http://' + window.location.host + ':3000/chat', socketOptions)
            })
          }

          socket.on('connect', function () {
            socket.on('error', function (err) {
              console.log('joinRoom error: ', err)
              defer.reject(err)
            })

            socket.emit('userJoin', {
              roomId: roomId,
              userId: rootScope.user._id
            })

            defer.resolve(socket)
          })

          return defer.promise
        },
        disconnect: function (roomId) {
          if (!socket) {
            return q.reject('No socket to exit')
          }

          socket.emit('userLeaving', {
            roomId: roomId,
            userId: rootScope.user._id
          })

          socket.disconnect(true)
          socket = null

          return q.when(true)
        },
        sendMessage: function (roomId, body) {
          if (!socket) {
            return q.reject('no active socket')
          }

          socket.emit('newMessage', {
            roomId: roomId,
            userId: rootScope.user._id,
            body: body
          })

          return q.when(true)
        }
      }
    }
  ])

})
(angular.module('chatClient'));
