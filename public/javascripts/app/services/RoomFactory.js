(function (module) {
  'use strict';

  module.service('RoomFactory', [
    '$http',
    function (http) {
      return {
        getAll: function () {
          return http.get('/rooms').then(window.toData)
        },
        getRoom: function (id) {
          return http.get('/rooms/' + id).then(window.toData)
        },
        createRoom: function (room) {
          return http.post('/rooms', room).then(window.toData)
        },
        deleteRoom: function (id) {
          return http.post('/rooms/delete/' + id).then(window.toData)
        }
      }
    }
  ])

})(angular.module('chatClient'));
