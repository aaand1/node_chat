(function (module) {
  'use strict';

  module.controller('HomeController', [
    '$scope', '$location', 'Rooms', 'RoomFactory',
    function (scope, location, Rooms, RoomFactory) {

      scope.rooms = Rooms

      scope.currentRoom = Rooms[0] || null

      scope.selectRoom = function (id) {
        location.path('/room/' + id)
      }

      scope.createRoom = function (name) {
        return RoomFactory.createRoom({name: name}).then(function (room) {
          return scope.selectRoom(room.name)
        })
      }

      scope.deleteRoom = function (id) {
        return RoomFactory.deleteRoom(id).then(function () {
          var index = -1

          id && scope.rooms.forEach(function (r, i) {
            if (r._id == id) {
              index = i
            }
          })

          if (index != -1) {
            return scope.rooms.splice(index, 1)
          }
        })
      }
    }
  ])

})(angular.module('chatClient'));
