(function (module) {
  'use strict';

  module.controller('PageController', [
    '$rootScope', '$location', '$http',
    function (rootScope, location, http) {
      rootScope.user = null

      rootScope.hasUser = function () {
        return !!(rootScope.user && rootScope.user.login)
      }

      rootScope.setUser = function (user) {
        return (rootScope.user = user)
      }

      rootScope.login = function (login) {
        return http.post('/users/login', { login: login }).then(window.toData)
          .then(rootScope.setUser)
      }

      rootScope.logout = function () {
        return http.post('/users/logout').then(function () {
          rootScope.setUser(null)
          location.path('/login')
        })
      }
    }
  ])

})(angular.module('chatClient'));
