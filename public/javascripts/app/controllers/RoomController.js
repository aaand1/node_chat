(function (module) {
  'use strict';

  module.controller('RoomController', [
    '$scope', 'ChatService', 'Room',
    function (scope, ChatService, Room) {
      scope.connected = false
      scope.room = Room

      scope.curMsg = {
        body: ''
      }

      ChatService.joinRoom(scope.room._id, {})
        .then(function (socket) {
          scope.connected = true

          if (scope.room.users.filter(function (u) {
              return u._id == scope.user._id
            }).length == 0) {
            scope.room.users.push(scope.user)
          }

          scope.room.users.forEach(function (user) {
            if (user.activeRooms.indexOf(scope.room._id) > -1) {
              user.online = true
            }
          })

          socket.on('userJoined', function (data) {
            data.online = true
            scope.room.users.push(data)
          })

          socket.on('userLeft', function (data) {
            var index = -1
            scope.room.users.forEach(function (u, i) {
              if (u._id == data.userId) {
                index = i
              }
            })
            if (index != -1) {
              scope.room.users.splice(index, 1)
              if (data.userId == scope.user._id) {
                location.path('/')
              }
            }
          })

          socket.on('userOnline', function (data) {
            scope.room.users.forEach(function (u) {
              if (u._id == data.userId) {
                u.online = true
              }
            })
          })

          socket.on('userOffline', function (data) {
            if (data.userId != scope.user._id) {
              scope.room.users.forEach(function (u) {
                if (u._id == data.userId) {
                  u.online = false
                }
              })
            }
          })

          socket.on('newMessage', function (data) {
            scope.room.messages.push(data)
          })

          socket.on('error', console.error.bind(console, 'ChatService::enterRoom socket#error: '))
        }, function (reason) {
          scope.connected = false
          console.error('ChatService::enterRoom error: ', reason)
        })

      scope.sendMessage = function (text) {
        ChatService.sendMessage(scope.room._id, text).then(function (data) {
          scope.curMsg.body = ''
        }).catch(console.error.bind(console))
      }

      scope.disconnect = function () {
        ChatService.exitRoom(scope.room._id).then(function () {
          scope.connected = false
        }).catch(console.error.bind(console))
      }

      scope.$on('$destroy', function () {
        ChatService.disconnect(scope.room._id)
      })
    }
  ])

})(angular.module('chatClient'));
