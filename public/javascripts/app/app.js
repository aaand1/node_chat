(function (exports) {
  'use strict';

  angular.module('chatClient', [
    'ngRoute', 'ngCookies',
    'btford.socket-io'
  ]).config([
    '$locationProvider', '$routeProvider',
    function (locationProvider, routeProvider) {
      locationProvider.html5Mode(false)

      routeProvider.when('/', {
        templateUrl: '/templates/home.html',
        controller: 'HomeController',
        resolve: {
          login: rejectToLoginOrLogin(),
          Rooms: [
            'RoomFactory',
            function (RoomFactory) {
              return RoomFactory.getAll()
            }
          ]
        }
      })

      routeProvider.when('/room/:id', {
        templateUrl: '/templates/room.html',
        controller: 'RoomController',
        resolve: {
          login: rejectToLoginOrLogin(),
          Room: [
            '$route', 'RoomFactory',
            function (route, RoomFactory) {
              return RoomFactory.getRoom(route.current.params.id)
            }
          ]
        }
      })

      routeProvider.when('/login', {
        templateUrl: '/templates/login.html',
        controller: [
          '$scope', '$location', function (scope, location) {
            scope.user = {name: ''}
            scope.login = function (user) {
              return scope.$root.login(user.name).then(function () {
                location.path('/')
              })
            }
          }
        ],
        resolve: {
          checkLogged: [
            '$http', '$location', function (http, location) {
              return http.get('/users/me')
                .then(window.toData)
                .then(function () {
                location.path('/')
                throw ''
              }, function (reason) {
                return reason
              })
            }
          ]
        }
      })

      function rejectToLoginOrLogin() {
        return [
          '$rootScope', '$http', '$location',
          function (rootScope, http, location) {
            return http.get('/users/me')
              .then(window.toData)
              .then(function (result) {
                rootScope.setUser(result)
                return result
              })
              .catch(function () {
                location.path('/login').replace()
                throw ''
              })
          }
        ]
      }
    }
  ]).run([
    function () {

    }
  ])

  exports.toData = function (result) {
    return result.data
  }

})(window);
